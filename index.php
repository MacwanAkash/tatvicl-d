<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>Tatvic L&D Internal</title>
  <!-- Favicon-->
  <link rel="icon" href="http://www.tatvic.com/favicon.ico" type="image/x-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

  <!-- Bootstrap Core Css -->
  <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

  <!-- Waves Effect Css -->
  <link href="plugins/node-waves/waves.css" rel="stylesheet" />

  <!-- Animation Css -->
  <link href="plugins/animate-css/animate.css" rel="stylesheet" />

  <!-- Morris Chart Css-->
  <link href="plugins/morrisjs/morris.css" rel="stylesheet" />

  <!-- Custom Css -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
  <link href="assets/css/themes/all-themes.css" rel="stylesheet" />

  <!--Fire Base CDN CSS  -->
  <!-- <link type="text/css" rel="stylesheet" href="https://www.gstatic.com/firebasejs/ui/2.3.0/firebase-ui-auth.css" /> -->
  <!-- <link href="assets/css/firebaseUIAuth.css" rel="stylesheet"/> -->
  <link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/1.0.0/firebaseui.css" />
  <link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->

    <!-- start of the container -->
      <div class="container">
        <!-- start of row -->
        <div class="row clearfix">
          <div class="col-md-4 col-md-offset-4">
                    <div class="card register-panel" style="background-color:#abaaa9;">
                      <div class="login-logo" style="margin-top: 15%;text-align: center;padding-top: 5%;"><img src="assets/images/TatvicLogo.png" style="height:75px;"></div>
                      <div class="header">
                        <h2>Please Sign In</h2>
                      </div>
                      <label></label>
                      <div class="body">
                        <?php
                          if(isset($_SESSION['errorMsg']))
                          echo "<span class=\"label label-danger\">".$_SESSION['errorMsg']."</span>";
                          if(isset($_SESSION['errorMsg'])){
                            unset($_SESSION['errorMsg']);
                          }
                         ?>
                        <div id="firebaseui-auth-container"></div>
                      </div>
                    </div>
                </div>
        <!-- End Of Row -->
          </div>
      <!-- End of Container -->
        </div>

        <!-- Below code is have very most priority do not remove its firebase config  -->
        <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>
        <!--Firebase CDN  -->
        <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-app.js"></script>
        <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-auth.js"></script>
        <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-database.js"></script>
        <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-messaging.js"></script>
        <script src='https://www.gstatic.com/firebasejs/ui/0.4.0/firebase-ui-auth.js'></script>

        <!-- Jquery Core Js -->
        <script src="plugins/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core Js -->
        <script src="plugins/bootstrap/js/bootstrap.js" defer></script>

        <!-- Select Plugin Js -->
        <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

        <!-- Slimscroll Plugin Js -->
        <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="plugins/node-waves/waves.js"></script>

        <!-- Jquery CountTo Plugin Js -->
        <script src="plugins/jquery-countto/jquery.countTo.js"></script>

        <!-- Morris Plugin Js -->
        <script src="plugins/raphael/raphael.min.js"></script>
        <script src="plugins/morrisjs/morris.js"></script>

        <script src="plugins/bootstrap-notify/bootstrap-notify.js"></script>

        <!-- Custom Js -->
        <script src="assets/js/admin.js"></script>
        <script src="plugins/sweetalert/sweetalert.min.js"></script>
        <script src="plugins/bootstrap-notify/bootstrap-notify.js"></script>
        <script src="assets/js/pages/ui/notifications.js"></script>
        <!-- Demo Js -->
        <script src="assets/js/demo.js"></script>


      <script>
        // Initialize Firebase

        var config = {
          apiKey: "AIzaSyAs0pnMESzqPMN8UlIHUsIzDEQSShNyZEU",
          authDomain: "tatvic-travela.firebaseapp.com",
          databaseURL: "https://tatvic-travela.firebaseio.com",
          projectId: "tatvic-travela",
          storageBucket: "tatvic-travela.appspot.com",
          messagingSenderId: "370782590047",
        };
        // firebase.initializeApp(config);
        var app = firebase.initializeApp(config);

        var uiConfig = {
          signInSuccessUrl: 'Home.php',
          signInOptions: [
            // Specify providers you want to offer your users.
            firebase.auth.GoogleAuthProvider.PROVIDER_ID,
            // firebase.auth.EmailAuthProvider.PROVIDER_ID
          ],
          // Terms of service url can be specified and will show up in the widget.
        };

        var auth = app.auth();
        var ui = new firebaseui.auth.AuthUI(auth);
        ui.start('#firebaseui-auth-container', uiConfig);



        firebase.auth().onAuthStateChanged(function(user) {
          if (user) {
            // User is signed in.
            var tvcAuthUser = "tvcAuthUser";
            var tvcUserEmail = user.email;
            var dataString = 'tvcAuthUser='+tvcAuthUser+"&tvcUserEmail="+tvcUserEmail;
              $.ajax({
                    type:"POST",
                    url:"LoginValidate.php",
                    data:dataString,
                    success: function(response) {

                      if(response=="FAILED"){
                          firebase.auth().signOut().then(function() {
                            // Sign-out successful.
                            // console.log('Successfully Sign Out');
                            // showNotification("","Please Register For Travela","bottom", "left", "","");
                            return false;
                          }).catch(function(error) {
                            // An error happened.
                            console.log("Error In Sign Out "+error);
                          });
                      }
                    }
                  });
                }
              });
      </script>

</body>

</html>
