<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Tatvic L&D Internal</title>
    <!-- Favicon-->
    <link rel="icon" href="http://www.tatvic.com/favicon.ico" type="image/x-icon">

    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/css/materialize.min.css"> -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">


    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">


    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="plugins/morrisjs/morris.css" rel="stylesheet" />



    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="assets/css/themes/all-themes.css" rel="stylesheet" />

    <!--Select -->
    <link href="plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Sweet Alerts -->
     <link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <link href="plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

</head>

<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="" alt="Tatvic Logo"><img src="assets/images/TatvicLogo.png" style="height: 175%;"></img></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="javascript:void(0);"  class="dropdown-toggle" data-toggle="dropdown" role="button">
                         <span id="spnUserName"><b></b></span>

                  </a>
                </li>

                <li class="dropdown">
                  <a href="javascript:void(0);" id="btnSignOut" class="dropdown-toggle" data-toggle="dropdown" role="button">
                         <i class="material-icons">exit_to_app</i>
                  </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->

   <!-- Jquery Core Js -->
   <script src="plugins/jquery/jquery.min.js" ></script>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/js/materialize.min.js"></script>

   <!-- Bootstrap Core Js -->
   <script src="plugins/bootstrap/js/bootstrap.js"></script>

   <!-- Select Plugin Js -->
   <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

   <!-- Slimscroll Plugin Js -->
   <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

   <!-- Waves Effect Plugin Js -->
   <script src="plugins/node-waves/waves.js"></script>

   <!-- Jquery CountTo Plugin Js -->
   <script src="plugins/jquery-countto/jquery.countTo.js"></script>

   <!-- Morris Plugin Js -->
   <script src="plugins/raphael/raphael.min.js"></script>
   <script src="plugins/morrisjs/morris.js"></script>


   <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

   <!-- Dropzone Plugin Js -->
   <script src="plugins/dropzone/dropzone.js"></script>

   <!-- Input Mask Plugin Js -->
   <script src="plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

   <!-- Demo Js -->
   <script src="assets/js/demo.js"></script>

   <!-- Custom Js -->
   <script src="assets/js/admin.js"></script>

   <!-- Below code is have very most priority do not remove its firebase config  -->
   <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>
   <!--Firebase CDN  -->
   <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-app.js"></script>
   <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-auth.js"></script>
   <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-database.js"></script>
   <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-messaging.js"></script>
   <script src='https://www.gstatic.com/firebasejs/ui/0.4.0/firebase-ui-auth.js'></script>
   <script src="plugins/sweetalert/sweetalert.min.js"></script>
   <script src="plugins/bootstrap-notify/bootstrap-notify.js"></script>
   <script src="assets/js/pages/ui/notifications.js"></script>

    <!-- Jquery Datatable -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <!-- End OF Jquery Datatable -->
   <script>

   $(document).ready(function(){
   $('textarea#txtTrainingDesc').characterCounter();

   var config = {
     apiKey: "AIzaSyAs0pnMESzqPMN8UlIHUsIzDEQSShNyZEU",
     authDomain: "tatvic-travela.firebaseapp.com",
     databaseURL: "https://tatvic-travela.firebaseio.com",
     projectId: "tatvic-travela",
     storageBucket: "tatvic-travela.appspot.com",
     messagingSenderId: "370782590047",
   };
   firebase.initializeApp(config);

   firebase.auth().onAuthStateChanged(function(user) {
     if (user) {
       // User is signed in.
       var welcomeMessage = "Welcome "+user.displayName;
         $("#spnUserName > b").text(welcomeMessage);
         $("#txtTrainerName").val(user.displayName);
     } else {
       // No user is signed in.
      //  window.location.href="http://localhost/L&D/index.php";
      console.log('From Header Not Signed IN');
     }
   });

   $("#btnSignOut").click(function(){
        var tvcSignOut = "tvcSignOut"
        var dataString = "tvcSignOut="+tvcSignOut;
        $.ajax({
          type:"POST",
          url:"LoginValidate.php",
          data:dataString,
          beforeSend: function () {
             console.log('Sending Request.......');
          },
          success:function(response){
            if(response=="success"){
              firebase.auth().signOut().then(function(user) {
                console.log('Signing Out.......');
                window.location.href="http://localhost/L&D/index.php";
              }, function(error) {
                console.error('Sign Out Error', error);
              });
            }else{
              console.log(response);
              console.log("FAIL TO LOG OUT");
            }
          }
        });
      });
   });
   </script>
<html>
