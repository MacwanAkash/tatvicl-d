<?php
session_start();
  if(!isset($_SESSION['email'])){
     header("location: index.php");
  }
?>

<style>
.divider{
  position: absolute;
  left: 50%;
  top: 15%;
  bottom: 10%;
  border-left: 1px solid rgba(148, 148, 148, 0.21);
  height: 57%
}
@media (max-width: 767px) {
    .divider {
        display:none;
    }
}
.bootstrap-select{
    z-index: auto !important;
}
</style>
<body class="theme-orange">
  <?php
  include_once('Header.php');
  ?>
  <!-- Page Loader -->
   <div class="page-loader-wrapper">
       <div class="loader">
           <div class="preloader">
               <div class="spinner-layer pl-red">
                   <div class="circle-clipper left">
                       <div class="circle"></div>
                   </div>
                   <div class="circle-clipper right">
                       <div class="circle"></div>
                   </div>
               </div>
           </div>
           <p>Please wait...</p>
       </div>
   </div>
   <!-- #END# Page Loader -->



   <section class="content">
     <div class="container-fluid">
       <div class="row clearfix">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="card">
             <div class="body">
               <!-- Nav tabs -->
               <ul class="nav nav-tabs tab-nav-right" role="tablist">
                 <li role="presentation" class="active"><a href="#training" data-toggle="tab">SCHEDULE TRAINING</a></li>
                 <!-- <li role="presentation"><a href="#profile" data-toggle="tab">PROFILE</a></li> -->
               </ul>
               <!-- Tab panes -->
               <div class="tab-content">
                 <div role="tabpanel" class="tab-pane fade in active" id="training">
                   <div class="row">
                     <form id="trainingForm">
                     <div class="col-md-6">

                       <div class="input-group input-group-lg">
                         <span class="input-group-addon">
                           <i class="material-icons">person</i>
                         </span>
                         <div class="form-line">
                           <input type="text" id="txtTrainerName" class="form-control" placeholder="TrainerName" readonly="">
                         </div>
                       </div>


                       <div class="input-group input-group-lg">
                         <span class="input-group-addon">
                           <i class="material-icons">date_range</i>
                         </span>
                         <div class="form-line">
                           <input type="text" id ="txtTrainingDate" class="form-control datepicker" placeholder="Trainig Date" required="" aria-required="true">
                         </div>
                       </div>

                       <div class="input-group input-group-lg">
                         <span class="input-group-addon ">
                           <i class="material-icons">access_time</i>
                         </span>
                         <div class="form-line">
                           <input type="text" class="form-control timepicker" placeholder="Start Time" id="txtStartTime">
                         </div>
                       </div>

                       <div class="input-group input-group-lg">
                         <span class="input-group-addon">
                           <i class="material-icons">access_time</i>
                         </span>
                         <div class="form-line">
                           <input type="text" class="form-control timepicker" placeholder="End Time" id="txtEndTime">
                         </div>
                       </div>

                     </div>

                     <div class="divider"></div>

                     <div class="col-md-6">

                       <div class="input-group input-group-lg">
                         <span class="input-group-addon">
                           <i class="material-icons">library_books</i>
                         </span>
                         <div class="form-line">
                           <input type="text" id="txtTopic" class="form-control" placeholder="Topic">
                         </div>
                       </div>



                       <div class="form-group">
                        <div class="input-group input-group-lg" id="divTrainingType">
                         <span class="input-group-addon">
                           <i class="material-icons">merge_type</i>
                         </span>
                         <div class="form-line">
                           <select class="form-control" id="slctType">
                             <option value=" ">Training Type</option>
                             <option value="Presentation">Presentation</option>
                             <option value="Practical">Practical</option>
                             <option value="Video">Video</option>
                             <option value="Hands On">Hands On</option>
                             <option value="All Methods">All Methods</option>
                         </select>
                         </div>
                       </div>
                      </div>

                       <div class="input-group input-group-lg">
                         <span class="input-group-addon">
                           <i class="material-icons">description</i>
                         </span>
                         <div class="form-line">
                             <textarea id="txtTrainingDesc" rows="3" data-length="120" class="form-control no-resize materialize-textarea" placeholder="Enter Description Here" id="txtDescription"></textarea>
                         </div>
                       </div>

                      <div class="col-md-3"></div>
                      <div class="col-md-3"></div>
                      <div class="col-md-3">
                        <!-- <button class="btn waves-effect waves-light" type="reset" name="action">
                          RESET
                        </button> -->
                      </div>
                     </div><!--End Of Col-md-6-->

                     <div class="col-md-4"></div>
                     <div class="col-md-4">
                       <button class="btn btn-block btn-primary waves-effect waves-light" type="button" name="btnSubmitTraining" id="btnSubmitTraining">
                         <span>SUBMIT </span>
                         <i class="material-icons">send</i>
                       </button>
                     </div>
                     <div class="col-md-4"></div>



                  </form>

                     </div>

                 </div>

                 <div role="tabpanel" class="tab-pane fade" id="profile">
                   <b>Profile Content</b>
                   <p>
                    Lorem ipsum dolor sit amet, ut duo atqui exerci dicunt, ius impedit mediocritatem an. Pri ut tation electram moderatius.
                    Per te suavitate democritum. Duis nemore probatus ne quo, ad liber essent aliquid
                    pro. Et eos nusquam accumsan, vide mentitum fabellas ne est, eu munere gubergren
                    sadipscing mel.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="card">
            <div class="header">
              <h2>  TRAINING DETAILS</h2>
            </div>

            <div class="body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="tblTrainingDetails">
                  <thead>
                  <tr>
                  <th>Trainer Name</th>
                  <th>Training Topic</th>
                  <th>Training Type</th>
                  <th>Description</th>
                  <th>Date</th>
                  <th>Start Time</th>
                  <th>End Time</th>
                  </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</body>

<script>
trainingTable = $("#tblTrainingDetails").DataTable({});

function showAjaxLoaderMessage(tname,tdate,startTime,endTime,topic,type,description) {
  swal({
      title: "Schedule Training?",
      text: "Submit To Schedule.",
      type: "info",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      confirmButtonText:'Schedule',
  }, function () {
     var tvcBtnSubmit = "tvcBtnSubmit";
     var dataString = "tvcBtnSubmit="+tvcBtnSubmit+"&tvcTname="+tname+"&tvcTdate="+tdate+"&tvcStartTime="+startTime+"&tvcEndTime="+endTime+"&tvcTopic="+topic+"&tvcType="+type+"&tvcDescription="+description;
     var insertTraining = hitAjaxRequest(dataString);
       insertTraining.success(function(response){
         if(response == 1){
            // swal("Training Scheduled!");
            setTimeout(function () {
                swal("Training Scheduled!");
            }, 1500);
            getRecords();
         }else{
            swal("OOPS! Can't Schedule Your Training");
         }
       });
       insertTraining.complete(function(){
         $("#txtTrainingDate").val("");
         $("#txtStartTime").val("");
         $("#txtEndTime").val("");
         $("#txtTopic").val("");
         $("#txtTrainingDesc").val("");
        });
  });
}

  function getRecords(){
    var tvcGetRec = "tvcGetRec";
    var dataString = "tvcGetRec="+tvcGetRec;
    var getRecReq = hitAjaxRequest(dataString);
    getRecReq.success(function(response){
       var recs = JSON.parse(response);
       FillDataTable(recs);
    });
  }

function hitAjaxRequest(dataString){
  return $.ajax({
    type:"POST",
    url:"Schedule.php",
    data:dataString,
  });
}

function FillDataTable(dataSet){
  trainingTable.destroy();
   trainingTable = $("#tblTrainingDetails").DataTable({
    data:dataSet,
    columns:[
     {title: "Trainer Name",data: "trainerName"},
     {title: "Training Topic",data: "Topic"},
     {title: "Training Type",data: "Type"},
     {title: "Description",data: "Description"},
     {title: "Date",data: "TrainingDate"},
     {title: "Start Time",data: "StartTime"},
     {title: "End Time",data: "EndTime"},
   ]
  });
}

$(document).ready(function(){
  getRecords();

  $('.datepicker').pickadate({
    format: 'yyyy-mm-dd',
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false // Close upon selecting a date,
  });

  $('.timepicker').pickatime({
    default: 'now', // Set default time: 'now', '1:30AM', '16:30'
    fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
    twelvehour: true, // Use AM/PM or 24-hour format
    donetext: 'OK', // text for done-button
    cleartext: 'Clear', // text for clear-button
    canceltext: 'Cancel', // Text for cancel-button
    autoclose: false, // automatic close timepicker
    ampmclickable: true, // make AM PM clickable

    aftershow: function(){} //Function for after opening timepicker
  });

  $("#btnSubmitTraining").click(function(){
    var tvcTrainerName = $("#txtTrainerName").val();
    var tvcTrainingDate = $("#txtTrainingDate").val();
    var tvcStartTime = $("#txtStartTime").val();
    var tvcEndTime = $("#txtEndTime").val();
    var tvcTopic = $("#txtTopic").val();
    var tvcType = $("#slctType :selected").val();
    var tvcDescription = $("#txtTrainingDesc").val();
    if(tvcTrainingDate=="" || tvcStartTime == "" || tvcEndTime == "" || tvcTopic == "" || tvcType == "" || tvcDescription == ""){
       showNotification("","Please Fill All Values","bottom", "left", "","");
    }else{
      showAjaxLoaderMessage(tvcTrainerName,tvcTrainingDate,tvcStartTime,tvcEndTime,tvcTopic,tvcType,tvcDescription);
    }
  });

});
</script>
