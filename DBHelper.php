<?php
include_once('config.php');

class DatabaseHandler{
  var $servername = HOST_NAME;
  var $dbname = DB_NAME;
	var $username = USER_NAME;
	var $password = PASS;

  function fetchRowsWithCondition($table,$field,$value){
    $res=false;
		$rows = null;
    try{
      $conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn->prepare("Select * from $table where $field=:value");
      $stmt->bindParam(':value', $value);
      $res=$stmt->execute();
      if($res)
					{
						$rows=$stmt->fetchAll();
					}

    }catch(PDOException $e){
     echo "Error: " . $e->getMessage();
    }
    $conn = null;
				return $rows;
  }

  function insertTraining($userID,$trainerName,$trainingDate,$startTime,$endTime,$topic,$type,$description)
			{
				$res=false;
				try
				{
					$conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
				      // set the PDO error mode to exception
					$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				      // prepare sql and bind parameters
					$stmt = $conn->prepare("INSERT INTO TrainingDetails(user_id,trainerName,TrainingDate,StartTime,EndTime,Topic,Type,Description) VALUES (:userid,:tname,:tdate,:st,:et,:topic,:type,:description)");
					$stmt->bindParam(':userid', $userID);
          $stmt->bindParam(':tname', $trainerName);
					$stmt->bindParam(':tdate', $trainingDate);
					$stmt->bindParam(':st',$startTime);
					$stmt->bindParam(':et', $endTime);
					$stmt->bindParam(':topic', $topic);
					$stmt->bindParam(':type', $type);
					$stmt->bindParam(':description', $description);
					$res=$stmt->execute();
				}
				catch(PDOException $e)
				{
					echo "Error: " . $e->getMessage();
				}
				$conn = null;
				return $res;
			}


      function fetchByLatestDate(){
        $res=false;
    		$rows = null;
        try{
          $conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          // $stmt = $conn->prepare("SELECT * FROM TrainingDetails WHERE TrainingDate IN (SELECT max(TrainingDate) FROM TrainingDetails)");
          $stmt = $conn->prepare("SELECT * FROM TrainingDetails WHERE TrainingDate >= SYSDATE() ORDER BY TrainingDate ASC LIMIT 10");
          $res=$stmt->execute();
          if($res)
    					{
    						$rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    					}

        }catch(PDOException $e){
         echo "Error: " . $e->getMessage();
        }
        $conn = null;
    				return $rows;
      }
}
?>
