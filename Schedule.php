<?php
 session_start();
 include_once('DBHelper.php');

  if(isset($_POST['tvcBtnSubmit'])){
      insertTrainings();
  }

  if(isset($_POST['tvcGetRec'])){
      getLatestDateRecords();
  }

  function insertTrainings(){
    $db=new DatabaseHandler();
    $userID = $_SESSION['user_id'];
    $trainerName = $_POST['tvcTname'];
    $trainingDate =$_POST['tvcTdate'];
    $startTime = date('h:i:s a', strtotime($_POST['tvcStartTime']));
    $endTime = date('h:i:s a', strtotime($_POST['tvcEndTime']));
    $topic = $_POST['tvcTopic'];
    $type = $_POST['tvcType'];
    $description = $_POST['tvcDescription'];
    $scheduleTraining =$db-> insertTraining($userID,$trainerName,$trainingDate,$startTime,$endTime,$topic,$type,$description);
    echo $scheduleTraining;
    exit(0);
  }

  function getLatestDateRecords(){
    $db=new DatabaseHandler();
    $latestRecords = $db->fetchByLatestDate();
    echo json_encode($latestRecords);
  }

?>
