-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 04, 2017 at 05:15 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `L&D`
--

-- --------------------------------------------------------

--
-- Table structure for table `USER`
--

CREATE TABLE `USER` (
  `user_id` int(5) NOT NULL,
  `email` varchar(35) NOT NULL,
  `password` varchar(20) NOT NULL,
  `andoid_id` varchar(20) NOT NULL,
  `role` varchar(15) NOT NULL,
  `fcm_token` varchar(200) DEFAULT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `USER`
--

INSERT INTO `USER` (`user_id`, `email`, `password`, `andoid_id`, `role`, `fcm_token`, `last_login`, `active`) VALUES
(1, 'akash.macwan@tatvic.com', '', 'd9576110b028e78e', '', NULL, '0000-00-00 00:00:00', 0),
(2, 'vatsal@tatvic.com', '', '59d40f9e910de676', '', 'dfxrJvaHcPQ:APA91bFn0lPt2WqjJOiauTwYMfZVXb3eSHy6EVclcbvEQ4x46v772RuV-HN_6jqfptr239BDxiF-KtGqMCV4oQOFOvcJ4XShjV1xp68yvaN3GQcssnJgetiWUttPpmo72E72YhoMflcB', '2017-08-03 19:23:02', 0),
(3, 'harsht@tatvic.com', '', 'd9576110b028e78d', '', NULL, '0000-00-00 00:00:00', 0),
(4, 'yaman@tatvic.com', '', '466829809d90dec5', '', NULL, '0000-00-00 00:00:00', 0),
(5, 'mayank@tatvic.com', '', '8afb45fa3a978ce4', '', NULL, '0000-00-00 00:00:00', 0),
(6, 'sarjak@tatvic.com', '', '4fa55d8eaf253273', '', NULL, '0000-00-00 00:00:00', 0),
(7, 'pankaj.b@tatvic.com', '', '374da016e1cc0a37', '', NULL, '0000-00-00 00:00:00', 0),
(8, 'rajashree.patel@tatvic.com', '', '886d6276176f147c', '', NULL, '0000-00-00 00:00:00', 0),
(9, 'devang@tatvic.com', '', 'c8a76fb321eb815e', '', NULL, '0000-00-00 00:00:00', 0),
(10, 'jigar@tatvic.com', '', '510c9bf8faab5ce2', '', NULL, '0000-00-00 00:00:00', 0),
(11, 'jenny@tatvic.com', '', '9ba77c2f29ea77cd', '', NULL, '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `USER`
--
ALTER TABLE `USER`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `USER`
--
ALTER TABLE `USER`
  MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
